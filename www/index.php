<!doctype html>
<html class="no-js" lang="fr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Titre de la page</title>
	<meta name="description" content="Description du contenu de la page">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

<!-- Add your site or application content here -->

<header role="banner">
	<div class="container">
		<p class="text-uppercase text-center">Découverte de NPM</p>
	</div>
</header>
<main role="main">
	<div class="container">


	</div>
</main>
<footer role="contentinfo">
	<div class="container">
		<p>
			Pied de page
		</p>
	</div>
</footer>

<script src="js/app.js"></script>

</body>

</html>